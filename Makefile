# Copyright (C) 2010-2019 Daniel Baumann <daniel.baumann@progress-linux.org>
#
# SPDX-License-Identifier: GPL-3.0+
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

SHELL := sh -e

PROJECT := progress-linux-meta-packages

all: build

clean:
	@# empty

build:
	@# empty

test:
	@# empty

install: install-doc install-man

install-doc:
	# install doc
	mkdir -p $(DESTDIR)/usr/share/doc/$(PROJECT)
	cp *.txt $(DESTDIR)/usr/share/doc/$(PROJECT)

install-man:
	# install manpages
	for SECTION in $$(seq 1 8); \
	do \
		if ls manpages/*.$${SECTION} > /dev/null 2>&1; \
		then \
			mkdir -p $(DESTDIR)/usr/share/man/man$${SECTION}; \
			cp manpages/*.$${SECTION} $(DESTDIR)/usr/share/man/man$${SECTION}; \
		fi; \
	done

uninstall: uninstall-doc uninstall-man

uninstall-doc:
	# uninstall doc
	rm -rf $(DESTDIR)/usr/share/doc/$(PROJECT)

uninstall-man:
	# uninstall manpages
	for SECTION in $$(seq 1 8); \
	do \
		for FILE in manpages/*.$${SECTION}; \
		do \
			rm -f $(DESTDIR)/usr/share/man/man$${SECTION}/$$(basename $${FILE}); \
		done; \
		rmdir --ignore-fail-on-non-empty --parents $(DESTDIR)/usr/share/man/man$${SECTION} || true; \
	done

reinstall: clean uninstall build install
