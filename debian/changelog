progress-linux-metapackages (20190701-24) sid; urgency=medium

  * Uploading to sid.
  * Prefering ttyd over shellinabox in progress-linux-
    container-server.

 -- Daniel Baumann <daniel.baumann@progress-linux.org>  Sun, 25 Oct 2020 16:30:28 +0100

progress-linux-metapackages (20190701-23) sid; urgency=medium

  * Uploading to sid.
  * Adding ioping to progress-linux-base-system depends.
  * Adding mtr-tiny to progress-linux-base-system depends.
  * Adding sysstat to progress-linux-base-system depends.
  * Adding tcpdump to progress-linux-base-system depends.
  * Adding numad to progress-linux-container-server depends.

 -- Daniel Baumann <daniel.baumann@progress-linux.org>  Sun, 04 Oct 2020 16:16:18 +0200

progress-linux-metapackages (20190701-22) sid; urgency=medium

  * Uploading to sid.
  * Adding htop to progress-linux-base-system depends.

 -- Daniel Baumann <daniel.baumann@progress-linux.org>  Thu, 03 Sep 2020 19:56:43 +0200

progress-linux-metapackages (20190701-21) sid; urgency=medium

  * Uploading to sid.
  * Moving deluge to progress-linux-desktop suggests.

 -- Daniel Baumann <daniel.baumann@progress-linux.org>  Sun, 30 Aug 2020 16:01:42 +0200

progress-linux-metapackages (20190701-20) sid; urgency=medium

  * Uploading to sid.
  * Moving knot-resolver related packages from progress-linux-base
    recommends to depends as they have been fixed now.
  * Removing htop from progress-linux-base depends.
  * Removing tree from progress-linux-base depends.
  * Removing xul-ext-quotecolors from progress-linux-desktop suggests.
  * Moving safe-rm from progress-linux-base depends to progress-linux-
    server depends.

 -- Daniel Baumann <daniel.baumann@progress-linux.org>  Fri, 31 Jul 2020 07:15:17 +0200

progress-linux-metapackages (20190701-19) sid; urgency=medium

  * Uploading to sid.
  * Adding nvme-cli to progress-linux-host depends.
  * Removing calibre from progress-linux-desktop depends.
  * Adding webext-privacy-badger to progress-linux-desktop depends.
  * Adding webext-umatrix to progress-linux-desktop suggests.
  * Adding exfatprogs als prefered alternative to exfat-fuse and exfat-
    utils in progress-linux-desktop depends.
  * Removing network-manager-openvpn from progress-linux-desktop depends.
  * Removing pidgin from progress-linux-desktop depends.
  * Making enigmail conditional on old thunderbird in progress-linux-
    desktop depends.
  * Removing scribus from progress-linux-desktop recommends and suggests.
  * Making enigmail conditional on new gnome-shell in progress-linux-
    gnome-desktop depends.
  * Removing network-manager-openvpn-gnome in progress-linux-gnome-desktop
    depends.
  * Harmonizing comments in control.

 -- Daniel Baumann <daniel.baumann@progress-linux.org>  Tue, 28 Jul 2020 19:36:10 +0200

progress-linux-metapackages (20190701-18) sid; urgency=medium

  * Uploading to sid.
  * Adding ethtool to progress-linux-host depends.

 -- Daniel Baumann <daniel.baumann@progress-linux.org>  Wed, 17 Jun 2020 17:42:46 +0200

progress-linux-metapackages (20190701-17) sid; urgency=medium

  * Uploading to sid.
  * Updating to debhelper version 13.
  * Excluding thinderbird and related packages on armhf and armel where
    they are currently unavaible.

 -- Daniel Baumann <daniel.baumann@progress-linux.org>  Sun, 19 Apr 2020 14:15:49 +0200

progress-linux-metapackages (20190701-16) sid; urgency=medium

  * Uploading to sid.
  * Adding cups as alternative in progress-linux-desktop depends (Closes:
    #954394).

 -- Daniel Baumann <daniel.baumann@progress-linux.org>  Sat, 21 Mar 2020 09:41:17 +0100

progress-linux-metapackages (20190701-15) sid; urgency=medium

  * Uploading to sid.
  * Dropping haveged from progress-linux-host, not needed with linux 5.4
    anymore.
  * Adding liferea to progress-linux-desktop depends.

 -- Daniel Baumann <daniel.baumann@progress-linux.org>  Tue, 17 Mar 2020 19:44:27 +0100

progress-linux-metapackages (20190701-14) sid; urgency=medium

  * Uploading to sid.
  * Moving fonts-powerline-extra from progress-linux-base-system to
    progress-linux-host.

 -- Daniel Baumann <daniel.baumann@progress-linux.org>  Mon, 09 Mar 2020 07:27:42 +0100

progress-linux-metapackages (20190701-13) sid; urgency=medium

  * Uploading to sid.
  * Adding suggests to fonts-powelrine-extra to progress-linux-base-
    system.

 -- Daniel Baumann <daniel.baumann@progress-linux.org>  Sat, 07 Mar 2020 11:59:05 +0100

progress-linux-metapackages (20190701-12) sid; urgency=medium

  * Uploading to sid.
  * Using execute_after targets in rules to be explicit what we're doing.
  * Adding socat to progress-linux-base-system recommends.

 -- Daniel Baumann <daniel.baumann@progress-linux.org>  Tue, 25 Feb 2020 15:36:00 +0100

progress-linux-metapackages (20190701-11) sid; urgency=medium

  * Uploading to sid.
  * Updating years in copyright file for 2020.
  * Updating to standards version 4.5.0.
  * Adding python3-debian to progress-linux-maintainers depends.
  * Moving xul-ext-quotecolors to progress-linux-desktop suggests.

 -- Daniel Baumann <daniel.baumann@progress-linux.org>  Sat, 01 Feb 2020 06:53:54 +0100

progress-linux-metapackages (20190701-10) sid; urgency=medium

  * Uploading to sid.
  * Moving gnome-shell-extension-multi-monitors back to progress-linux-
    gnome-desktop depends as it's in testing again.

 -- Daniel Baumann <daniel.baumann@progress-linux.org>  Mon, 30 Dec 2019 06:41:27 +0100

progress-linux-metapackages (20190701-9) sid; urgency=medium

  * Uploading to sid.
  * Adding htop to progress-linux-base-system depends.
  * Adding tree to progress-linux-base-system depends.
  * Adding intel-isdct to progress-linux-host suggests.

 -- Daniel Baumann <daniel.baumann@progress-linux.org>  Tue, 10 Dec 2019 21:04:48 +0100

progress-linux-metapackages (20190701-8) sid; urgency=medium

  * Uploading to sid.
  * Moving scribus depends in progress-linux-desktop to recommends
    (currently not in testing).
  * Moving knot and knot-resolver depends in progress-linux-base-system to
    recommends (currently not in testing).
  * Adding alternative depends www-browser to firefox and firefox-esr to
    ease testing migration (firefox-esr missing on armel).

 -- Daniel Baumann <daniel.baumann@progress-linux.org>  Mon, 18 Nov 2019 14:57:05 +0100

progress-linux-metapackages (20190701-7) sid; urgency=medium

  * Uploading to sid.
  * Also running diversion on upgrade in progress-linux-container package.

 -- Daniel Baumann <daniel.baumann@progress-linux.org>  Mon, 18 Nov 2019 09:51:08 +0100

progress-linux-metapackages (20190701-6) sid; urgency=medium

  * Uploading to sid.
  * Updating to standards version 4.4.1.
  * Adding conflicts against resolvconf in progress-linux-base.
  * Diverting shutdown commands in progress-linux-container package.
  * Using codenames as upload targets in changelog.

 -- Daniel Baumann <daniel.baumann@progress-linux.org>  Mon, 18 Nov 2019 09:07:39 +0100

progress-linux-metapackages (20190701-5) sid; urgency=medium

  * Uploading to sid.
  * Adding gpm to progress-linux-host depends.
  * Moving fonts-powerline from progress-linux-system to progress-linux-
    desktop.

 -- Daniel Baumann <daniel.baumann@progress-linux.org>  Thu, 19 Sep 2019 20:05:09 +0200

progress-linux-metapackages (20190701-4) sid; urgency=medium

  * Uploading to sid.
  * Adding lynx to progress-linux-base-system.
  * Moving spectre-meltdown-checker from progress-linux-server to
    progress-linux-host.

 -- Daniel Baumann <daniel.baumann@progress-linux.org>  Fri, 13 Sep 2019 20:59:30 +0200

progress-linux-metapackages (20190701-3) sid; urgency=medium

  * Uploading to sid.
  * Adding netdata-apache2 and netdata-plugins-python to progress-linux-
    container-server.
  * Only adding knot-resolver for supported architectures in progress-
    linux-base-system depends.
  * Only adding browserpass for supported architectures in progress-linux-
    desktop depends.
  * Only adding thunderbird (and extensions) for supported architectures
    in progress-linux-desktop depends.
  * Only adding dmidecode for supported architectures in progress-linux-
    host depends.
  * Downgrading gnome-shell-extension-multi-monitors to recommends for
    progress-linux-gnome-desktop until it migrates to testing.
  * Marking metapackages with architecture-specific depends as
    architecture any.

 -- Daniel Baumann <daniel.baumann@progress-linux.org>  Tue, 27 Aug 2019 22:01:56 +0200

progress-linux-metapackages (20190701-2) sid; urgency=medium

  * Uploading to sid.
  * Adding graphviz to progress-linux-maintainers depends.
  * Adding dput to progress-linux-maintainers depends.

 -- Daniel Baumann <daniel.baumann@progress-linux.org>  Sun, 11 Aug 2019 10:36:37 +0200

progress-linux-metapackages (20190701-1) sid; urgency=medium

  * Uploading to sid.
  * Merging upstream version 20190701.
  * Adding firmware-ast to progress-linux-host suggests.
  * Adding supermicro-ipmicfg to progress-linux-host suggests.
  * Adding safe-rm to progress-linux-base-system depends.
  * Adding myrepos to progress-linux-base-maintainers depends.
  * Adding sphinx to progress-linux-base-maintainers depends.

 -- Daniel Baumann <daniel.baumann@progress-linux.org>  Sun, 30 Jun 2019 08:03:28 +0200

progress-linux-metapackages (20190225-5) sid; urgency=medium

  * Uploading to sid.
  * Adding hdparm to progress-linux-host depends.
  * Adding spectre-meltdown-checker to progress-linux-container-server
    depends.
  * Depending on cryptsetup-bin instead of cryptsetup in progress-linux-
    host.

 -- Daniel Baumann <daniel.baumann@progress-linux.org>  Tue, 16 Apr 2019 13:24:58 +0200

progress-linux-metapackages (20190225-4) sid; urgency=medium

  * Uploading to sid.
  * Adding gnome-shell-extension-bluetooth-quick-connect to progress-
    linux-gnome-desktop depends.

 -- Daniel Baumann <daniel.baumann@progress-linux.org>  Wed, 27 Mar 2019 05:59:58 +0100

progress-linux-metapackages (20190225-3) sid; urgency=medium

  * Uploading to sid.
  * Adding lnav to progress-linux-base-system depends.
  * Adding crudini to progress-linux-base-system depends.

 -- Daniel Baumann <daniel.baumann@progress-linux.org>  Sat, 16 Mar 2019 10:13:09 +0100

progress-linux-metapackages (20190225-2) sid; urgency=medium

  * Uploading to sid.
  * Updating URL in watch file.
  * Adding systemd-sysv to progress-linux-container depends.
  * Adding smartmontools to progress-linux-host depends.

 -- Daniel Baumann <daniel.baumann@progress-linux.org>  Wed, 27 Feb 2019 22:02:05 +0100

progress-linux-metapackages (20190225-1) sid; urgency=medium

  * Initial upload to sid.

 -- Daniel Baumann <daniel.baumann@progress-linux.org>  Mon, 25 Feb 2019 19:12:16 +0100
